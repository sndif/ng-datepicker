angular
  .module 'ngDatepicker', []
  .directive 'ngDatepicker', [ ->
    {
      restrict : 'A'
      require : [
        'ngModel'
      ]
      scope :
        ngModel   : '='
        format    : '@'
        future    : '='
        past      : '='
        timezone  : '@'
      link : ( $scope, $elem, attrs, ctrls ) ->
        # make readonly
        $elem.attr 'readonly', 'true'

        # default values
        $scope.format   = 'YYYY-MM-DD' if not $scope.format
        $scope.timezone = 'America/Mexico_City' if not $scope.timezone

        # convert ngModel to momento date
        $scope.ngModel = moment( $scope.ngModel, $scope.format ).format $scope.format
        $scope.ngModel = '' if $scope.ngModel is 'Invalid date'

        future = if $scope.future is false then false else true
        past   = if $scope.past is false then false else true

        options =
          format : $scope.format.toLowerCase()
          closeButton : false
          language : 'es'

        # limits
        if not future
          options.endDate = moment( new Date(), $scope.timezone ).format $scope.format
        if not past
          options.startDate = moment( new Date(), $scope.timezone ).format $scope.format

        $elem.fdatepicker options

    }
  ]

